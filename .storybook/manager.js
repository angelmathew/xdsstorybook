import { addons } from '@storybook/addons';
import BellTheme from './bellTheme';
import { themes } from '@storybook/theming/create';

addons.setConfig({
  theme: BellTheme,
});