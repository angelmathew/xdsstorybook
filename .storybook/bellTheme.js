import { create } from '@storybook/theming/create';

export default create({
  base: 'light',

  colorPrimary: '#00549A',
  colorSecondary: '#003778',
  
  // UI
  appBg: '#F4F4F4',
  appContentBg: 'white',
  appBorderColor: '#CCCCCC',
  appBorderRadius: 0,

  // Typography
  fontBase: '"Arial", sans-serif',
  fontCode: 'monospace',

  // Text colors
  textColor: '#111111',
  textInverseColor: 'white',

  // Toolbar default and active colors
  barTextColor: 'white',
  barSelectedColor: 'white',
  barBg: '#00549A',

  // Form colors
  inputBg: 'white',
  inputBorder: '#CCCCCC',
  inputTextColor: '#111111',
  inputBorderRadius: 4,

  brandTitle: 'Bell XDS',
  brandUrl: 'https://example.com',
  brandImage: './assets/logo/BellLogo.svg',
});