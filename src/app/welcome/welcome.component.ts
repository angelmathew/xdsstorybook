import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'storybook-welcome-component',
  template: `
    <main>
      <h1>XDS Design System</h1>
      <p>This is the Bell XDS Design System. <a href="#">link</a></p>
    </main>
  `,
  styles: [
    `
      main {
        padding: 15px;
        line-height: 1.4;
        font-family: 'Helvetica Neue', Helvetica, 'Segoe UI', Arial, freesans, sans-serif;
        background-color: #ffffff;
      }
      .note {
        opacity: 0.5;
      }
      .inline-code {
        font-size: 15px;
        font-weight: 600;
        padding: 2px 5px;
        border: 1px solid #eae9e9;
        border-radius: 4px;
        background-color: #f3f2f2;
        color: #3a3a3a;
      }
      a {
        padding-bottom: 2px;
      }
    `,
  ],
})
export class WelcomeComponent {
  @Output()
  showApp = new EventEmitter<any>();
}