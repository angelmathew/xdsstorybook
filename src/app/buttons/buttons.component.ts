import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'button-primary',
  template: `
    <button class="button button--primary" [disabled]="disabled" (click)="onClickTask.emit($event)">
      {{text}}
    </button>
  `,
  styleUrls: ['./buttons.component.scss']
})
export class ButtonsComponent {
  @Input() text: string;
  @Input() disabled: boolean;
  @Output() onClickTask: EventEmitter<any> = new EventEmitter();
}