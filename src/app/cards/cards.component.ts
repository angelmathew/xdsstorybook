import {Component, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'cards-basic',
  templateUrl: './cards.component.html',
//   template: `
  
//     <div class="cards cards-basic"  (click)="onClickTask.emit($event)">
//     <div class="cards__title">
//     <div class="cards__title__text">Security Status</div>
//     <div class="cards-dots">
//     <span></span>
//     <span></span>
//     <span></span>
//     </div>

//     </div>
//     </div>
//   `,
  styleUrls: ['./cards.component.scss']
})
export class CardsComponent {
  @Input() text: string;
  @Input() disabled: boolean;
  @Output() onClickTask: EventEmitter<any> = new EventEmitter();
}