import { withKnobs, boolean } from '@storybook/addon-knobs';
import {NavigationComponent} from '../app/navigation/navigation.component';
import { storiesOf } from '@storybook/angular';
import {AppModule} from '../app/app.module';
// import { centered } from '@storybook/addon-centered/angular';
/*********************** Story Module ***********************/
export default {
  title: 'Navigation',
  component: NavigationComponent, 
  decorators: [withKnobs]
};

// button--primary--default 
export const NavigationBasic = () => ({ 
  component: NavigationComponent, 
  name: 'Navigation Basic',
  props: {
    text: 'Navigation',
    disabled: boolean('disabled', false)
  }
});

// button--primary--default--icon-only 
export const NavigationStatus = () => ({ 
    component: NavigationComponent,
    name: 'Navigation Status',
    templateUrl: '../app/navigation/navigation.component.html',

    styleUrls: ['../app/navigation/navigation.component.scss'],
    props: {
      text: '',
      disabled: boolean('disabled', false)
    },
});

/*********************** Documentation ***********************/
NavigationBasic.story = {
    parameters: {
        notes: 'Documentation of Cards Basic',
    }
};
NavigationBasic.story = {
    parameters: {
        notes: 'Documentation of Statuses Options Linktitle',
    }
};

// storiesOf('Addon|Centered', module)
//   .addDecorator(centered)
//   .add('centered component', () => ({
//     component: CardsComponent,
//     props: {},
//   }));