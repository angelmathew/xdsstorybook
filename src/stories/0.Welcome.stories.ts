import { withKnobs } from '@storybook/addon-knobs';
import { WelcomeComponent } from '../app/welcome/welcome.component';

/*********************** Story Module ***********************/
export default {
  title: 'Welcome to XDS',
  component: WelcomeComponent, 
  decorators: [withKnobs]
};

export const Welcome = () => ({ 
    component: WelcomeComponent, 
    name: 'Welcome to XDS',
  });