import { withKnobs, boolean } from '@storybook/addon-knobs';
import {CardsComponent} from '../app/cards/cards.component';
import { storiesOf } from '@storybook/angular';
import {AppModule} from '../app/app.module';
// import { centered } from '@storybook/addon-centered/angular';
/*********************** Story Module ***********************/
export default {
  title: 'Cards',
  component: CardsComponent, 
  decorators: [withKnobs]
};

// button--primary--default 
export const CardsBasic = () => ({ 
  component: CardsComponent, 
  name: 'Cards Basic',
  props: {
    text: 'Cards',
    disabled: boolean('disabled', false)
  }
});

// button--primary--default--icon-only 
export const CardsStatus = () => ({ 
    component: CardsComponent,
    name: 'Cards Status',
    templateUrl: '../app/cards/cards-status.component.html',

    styleUrls: ['../app/cards/cards.component.scss'],
    props: {
      text: '',
      disabled: boolean('disabled', false)
    },
});

/*********************** Documentation ***********************/
CardsBasic.story = {
    parameters: {
        notes: 'Documentation of Cards Basic',
    }
};
CardsStatus.story = {
    parameters: {
        notes: 'Documentation of Statuses Options Linktitle',
    }
};

// storiesOf('Addon|Centered', module)
//   .addDecorator(centered)
//   .add('centered component', () => ({
//     component: CardsComponent,
//     props: {},
//   }));