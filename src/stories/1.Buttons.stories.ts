import { withKnobs, boolean } from '@storybook/addon-knobs';
import {ButtonsComponent} from '../app/buttons/buttons.component';

/*********************** Story Module ***********************/
export default {
  title: 'Buttons',
  component: ButtonsComponent, 
  decorators: [withKnobs]
};

// button--primary--default 
export const DefaultPrimary = () => ({ 
  component: ButtonsComponent, 
  name: 'Button Primary',
  props: {
    text: 'Default',
    disabled: boolean('disabled', false)
  }
});

// button--primary--default--icon-only 
export const DefaultIconOnlyPrimary = () => ({ 
    component: ButtonsComponent,
    name: 'Button Primary Default Icon Only',
    template: `
        <button class="button button--primary icon-only" [disabled]="disabled" (click)="onClickTask.emit($event)">
            <svg viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M5.33334 2.66667L10.3333 8L5.33334 13.3333" stroke="white" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
            </svg>
        </button>
    `,
    styleUrls: ['../app/buttons/buttons.component.scss'],
    props: {
      text: '',
      disabled: boolean('disabled', false)
    },
});

/*********************** Documentation ***********************/
DefaultPrimary.story = {
    parameters: {
        notes: 'Documentation of default primary button',
    }
};
DefaultIconOnlyPrimary.story = {
    parameters: {
        notes: 'Documentation of default icon only primary button',
    }
};