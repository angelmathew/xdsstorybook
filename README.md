# XDS Component Library

A Bell component library developed by FSXD.

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.26.

## Development server

Run `npm run storybook` for a dev server. Navigate to the localhost url generated in from the command. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component.

